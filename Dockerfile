FROM node:8

WORKDIR /usr/src/hello-world

COPY package*.json ./
RUN npm install --silent

COPY . .

ENV NODE_ENV production

EXPOSE 8080

CMD ["npm","start"]